<?php


namespace app\models;
use Yii;
use yii\helpers\Json;

/**
 * Class Cache
 * @package app\models
 */
class Cache
{
    public function addKey($id)
    {
        $res = Yii::$app->cache->get($id);
        if (empty($res)) {
            $res = Yii::$app->cache->add($id, Json::decode(Order::find()->where(['id' => intval($id)])->indexBy('id')->asArray()->one(), true)
                , 21600);
        }
    }

    public function updateKey($id, $order)
    {
        $id    = strval($id);
        $cache = Yii::$app->cache;
        if (Yii::$app->cache->get($id)) {
            Yii::$app->cache->delete($id);
        }
        $res = Yii::$app->cache->add($id, $order
            , 21600);

    }

    public function getKey($id)
    {
        $id    = strval($id);
        $model = Yii::$app->cache->get($id);
        if ($model) {
            return $model;
        }
        return false;
    }

}
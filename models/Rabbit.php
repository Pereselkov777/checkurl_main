<?php


namespace app\models;

/**
 * Class Rabbit
 * @package app\models
 */
class Rabbit
{
    public const DEMO_PRODUCER     = 'DEMO_PRODUCER';
    public const DEMO_ROUTING_KEY  = 'DEMO_ROUTING_KEY';
    public const DEMO_ROUTING_KEY2 = 'DEMO_ROUTING_KEY2';
    public const DEMO_EXCHANGE     = 'DEMO_EXCHANGE';

}
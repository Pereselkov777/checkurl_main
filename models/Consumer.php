<?php


namespace app\models;

use mikemadisonweb\rabbitmq\components\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use yii\helpers\Json;

/**
 * Class Consumer
 * @package app\models
 */
class Consumer implements ConsumerInterface
{

    public function execute(AMQPMessage $msg)
    {
        var_dump(Json::decode($msg->getBody()));
        $data = Json::decode($msg->getBody());
        $config = $data["data"];
        $client = new Request($config);
        $client->getResponse(Request::URL_CRON);
        return ConsumerInterface::MSG_ACK;
    }
}
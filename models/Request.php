<?php


namespace app\models;

use GuzzleHttp\Exception\ClientException;
use Yii;

/**
 * Class Request
 * @package app\models
 */
class Request extends \yii\base\BaseObject
{
    public $params;
    public $base_url;
    public $frequency;
    public $repeat_count;
    public $url;
    public $url_id;
    public $check_id;
    public const URL_CRON = 'tester/cron';
    public const PARAMS_FREQUENCY = 'frequency';
    public const PARAMS_REPEAT_COUNT = 'repeat_count';
    public const PARAMS_URL = 'url';
    public const PARAMS_URL_ID = 'url_id';
    public const PARAMS_CHECK_ID = 'check_id';
    public $client;


    /**
     * @param array $config
     * @param int   $cluster_id
     */
    public function __construct( array $config = [])
    {
        $this->params = $config;
        $this->base_url = 'http://127.0.0.1:8085/api/';
        parent::__construct($config);
    }


    public function getResponse( string $go_controller_url)
    {
        $this->client = new \GuzzleHttp\Client([
            'base_uri' => $this->base_url,
        ]);
        try {

            $response = $this->client->request('GET', $go_controller_url , [
                'headers' => ['worker-access-token' => Yii::$app->params['worker-access-token']],
                'query' => $this->params
            ]);

            $rows = json_decode((string)$response->getBody(), true);

            return $rows;
        } catch (ClientException $e) {
            return $e->getCode();
        }
    }
}
<?php

namespace app\models;

use app\models\Rabbit;


use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%check}}".
 *
 * @property int $id
 * @property string|null $created_at
 * @property int $url_id
 * @property int $frequency
 * @property int $repeat_count
 *
 * @property Checklog[] $checklogs
 * @property Url $url
 */
class Check extends \yii\db\ActiveRecord
{
    public $url;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%check}}';
    }


    public static function addCheck($params, $url_id)
    {
        $check = new Check();
        $check->url_id = $url_id;
        $check->frequency = intval( $params['frequency']);
        $check->repeat_count = intval($params['repeat_count']);
        if (!$check->save()){
           Yii::$app->getSession()->setFlash('danger', $check->getErrors('url_id')[0]);
            return false;
        }
        $rabbitmq = \Yii::$app->rabbitmq;
        $producer = $rabbitmq->getProducer(Rabbit::DEMO_PRODUCER);
        $msg = Json::encode(['data_id' => 'DEMO_ROUTING_KEY2' . rand(1, 1000), 'data'=> [
            Request::PARAMS_FREQUENCY => $check->frequency,
            Request::PARAMS_REPEAT_COUNT => $check->repeat_count,
            Request::PARAMS_URL => $params['url'],
            Request::PARAMS_URL_ID => $url_id,
            Request::PARAMS_CHECK_ID => $check->id  ]]);
        $producer->publish($msg, Rabbit::DEMO_EXCHANGE, Rabbit::DEMO_ROUTING_KEY2);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['url_id', 'frequency', 'repeat_count'], 'required'],
            [['url_id', 'frequency', 'repeat_count'], 'integer'],
            [['url_id'], 'exist', 'skipOnError' => true, 'targetClass' => Url::className(), 'targetAttribute' => ['url_id' => 'id']],
            [['url_id', 'frequency', 'repeat_count'], 'unique', 'targetAttribute' => ['url_id', 'frequency', 'repeat_count']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'url_id' => 'Url ID',
            'frequency' => 'Частота',
            'repeat_count' => 'Количество повторов',
        ];
    }

    /**
     * Gets query for [[Checklogs]].
     *
     * @return \yii\db\ActiveQuery|
     */
    public function getChecklogs()
    {
        return $this->hasMany(Checklog::className(), ['check_id' => 'id']);
    }

    /**
     * Gets query for [[Url]].
     *
     * @return \yii\db\ActiveQuery|
     */
    public function getUrl()
    {
        return $this->hasOne(Url::className(), ['id' => 'url_id']);
    }

    public static function getFrequencyList()
    {
        return [
            1 => 1,
            5 => 5,
            10 => 10
        ];
    }
    public static function getRepeatCountList()
    {
        return [
            0 => 0,
            1 => 1,
            2 => 2,
            3 => 3,
            4 => 4,
            5 => 5,
            6 => 6,
            7 => 7,
            8 => 8,
            9 => 9,
            10 => 10
        ];
    }
}

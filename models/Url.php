<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%url}}".
 *
 * @property int $id
 * @property string $url
 * @property string|null $created_at
 *
 * @property Checklog[] $checklogs
 * @property Check[] $checks
 */
class Url extends \yii\db\ActiveRecord
{
    public $frequency;
    public $repeat_count;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%url}}';
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'frequency', 'repeat_count'], 'required'],
            [['created_at'], 'safe'],
            ['url', 'url', 'defaultScheme' => 'https'],
            [['frequency', 'repeat_count'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Checklogs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChecklogs()
    {
        return $this->hasMany(Checklog::className(), ['url_id' => 'id']);
    }

    /**
     * Gets query for [[Checks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChecks()
    {
        return $this->hasMany(Check::className(), ['url_id' => 'id']);
    }



    public static function getUrlById($id)
    {
        $model = self::findOne($id);
        return $model->url;
    }
}

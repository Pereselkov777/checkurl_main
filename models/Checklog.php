<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%checklog}}".
 *
 * @property int $id
 * @property string|null $created_at
 * @property int $url_id
 * @property int $check_id
 * @property int $http_code
 *
 * @property Check $check
 * @property Url $url
 *
 *
 */
class Checklog extends \yii\db\ActiveRecord
{

    public $url_string;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%checklog}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['url_id', 'check_id', 'http_code'], 'required'],
            [['url_id', 'check_id', 'http_code'], 'integer'],
            [['url_id'], 'exist', 'skipOnError' => true, 'targetClass' => Url::className(), 'targetAttribute' => ['url_id' => 'id']],
            [['check_id'], 'exist', 'skipOnError' => true, 'targetClass' => Check::className(), 'targetAttribute' => ['check_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'url_id' => 'Url ID',
            'check_id' => 'Check ID',
            'http_code' => 'Http Code',
            'errors_count' => 'Количество повторных попыток'
        ];
    }

    /**
     * Gets query for [[Check]].
     *
     * @return \yii\db\ActiveQuery|
    public function getCheck()
    {
        return $this->hasOne(Check::className(), ['id' => 'check_id']);
    }

    /**
     * Gets query for [[Url]].
     *
     * @return \yii\db\ActiveQuery|
     */
    public function getUrl()
    {
        return $this->hasOne(Url::className(), ['id' => 'url_id']);
    }


}

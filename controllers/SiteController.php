<?php

namespace app\controllers;


use app\commands\DemoProducerController;
use app\models\Check;
use app\models\Checklog;
use app\models\Request;
use Yii;
use yii\helpers\Json;

use app\models\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {


        $model = new Url();
        if ($this->request->isPost) {
            $url_id = null;
            $url =  Yii::$app->request->post()['Url']['url'];
            $model = Url::find()->where(['url'=> $url])->one();
            if ($model){
               $url_id = $model->id;
            } else {
                $model = new Url();
                if ($model->load($this->request->post()) && $model->save() ) {
                    $url_id = $model->id;
                } else {
                    foreach ($model->getErrors() as $error) {
                        Yii::$app->getSession()->setFlash('danger', $error);
                    }
                }
            }
            if (Check::addCheck(Yii::$app->request->post()["Url"], $url_id)){
                Yii::$app->getSession()->setFlash('success', 'Success');
            }
            return $this->refresh();

        } /*else {
            $model->loadDefaultValues();
        }*/

        return $this->render('index', ['model'=> $model]);
    }


}

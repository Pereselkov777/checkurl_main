<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
/** @var yii\web\View $this */
/** @var app\models\Url $model */

$this->title = 'My Yii Application';

?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'url')->textInput() ?>

<?= $form->field($model, 'frequency')->textInput()->dropDownList(\app\models\Check::getFrequencyList())  ?>

<?= $form->field($model, 'repeat_count')->textInput()->dropDownList(\app\models\Check::getRepeatCountList())  ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>
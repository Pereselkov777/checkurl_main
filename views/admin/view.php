<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $model app\models\Check */


$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Urls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?= GridView::widget([
    'dataProvider' => $model,
   // 'filterModel'  => $searchModel,
    'columns'      => [
        'frequency',
        'url_id',
        'repeat_count',
        [
            'class'          => ActionColumn::className(),
            'visibleButtons' => [
                'update' => false,
                'delete' => false,
            ],
            'urlCreator'     => function ($action, \app\models\Check $model, $key, $index, $column) {
                return Url::toRoute(['/admin/checklog', 'id' => $model->id]);
            }
        ],
    ],
]); ?>

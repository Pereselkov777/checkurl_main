<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $model app\models\Checklog */


$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Urls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<?= GridView::widget([
    'dataProvider' => $model,
    // 'filterModel'  => $searchModel,
    'columns'      => [
        'created_at',
        'http_code',
        'errors_count',
        'url_string',

        [
            'class'          => ActionColumn::className(),
            'visibleButtons' => [
                'update' => false,
                'delete' => false,
                'view' => false
            ],

        ],
    ],
]); ?>

<?php

use yii\db\Migration;

/**
 * Class m220623_102409_add_errors_count
 */
class m220623_102409_add_errors_count extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%checklog}}', "errors_count", $this->integer(10)->notNull()->defaultValue(0) );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%checklog}}', "errors_count");

        return false;
    }

}

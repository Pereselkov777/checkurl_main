<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%url}}`.
 */
class m220622_190531_create_url_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('{{%url}}', [
            'id' => $this->primaryKey()->notNull(),
            'url' => $this->string(255)->notNull(),
            // 'frequency' => $this->integer(2)->unsigned()->notNull(),
            // 'repeat_count' => $this->integer(2)->unsigned()->notNull(),
            'created_at'  => $this->dateTime() . ' DEFAULT NOW()',
        ]);
        $this->createIndex('url', 'url', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%url}}');
    }
}

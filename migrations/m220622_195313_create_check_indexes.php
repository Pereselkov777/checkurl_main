<?php

use yii\db\Migration;

/**
 * Class m220622_195313_create_check_indexes
 */
class m220622_195313_create_check_indexes extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-checklog3-check_id',
            'checklog',
            'check_id'
        );

        $this->addForeignKey(
            'fk-checklog4-check_id',
            'checklog',
            'check_id',
            'check',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220622_195313_create_check_indexes cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220622_195313_create_check_indexes cannot be reverted.\n";

        return false;
    }
    */
}

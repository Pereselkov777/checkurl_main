<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%checklog}}`.
 */
class m220622_193413_create_checklog_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%checklog}}', [
            'id'          => $this->primaryKey()->notNull(),
            'created_at'  => $this->dateTime() . ' DEFAULT NOW()',
            'url_id'      => $this->integer(10)->notNull(),
            'check_id'      => $this->integer(10)->notNull(),
            'http_code' => $this->integer(3)->notNull(),
        ]);
        $this->createIndex('url_check', 'checklog', 'id');
        $this->createIndex(
            'idx-checklog-url_id',
            'checklog',
            'url_id'
        );

        $this->addForeignKey(
            'fk-checklog-url_id',
            'checklog',
            'url_id',
            'url',
            'id',
            'CASCADE'
        );
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%checklog}}');
    }
}

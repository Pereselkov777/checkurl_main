<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%check}}`.
 */
class m220622_191310_create_check_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%check}}', [
        'id'          => $this->primaryKey()->notNull(),
        'created_at'  => $this->dateTime() . ' DEFAULT NOW()',
        'url_id'      => $this->integer(10)->notNull(),
        'frequency' => $this->integer(2)->unsigned()->notNull(),
        'repeat_count' => $this->integer(2)->unsigned()->notNull(),
        ]);
        $this->createIndex('url', 'check', 'id');
        $this->createIndex(
        'idx-check-url_id',
        'check',
        'url_id'
        );

        $this->addForeignKey(
        'fk-check-url_id',
        'check',
        'url_id',
        'url',
        'id',
        'CASCADE'
        );
    }

/**
 * {@inheritdoc}
 */
public function safeDown()
{
    $this->dropTable('{{%check}}');
}
}

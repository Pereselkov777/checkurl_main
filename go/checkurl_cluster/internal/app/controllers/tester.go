package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/go-co-op/gocron"
	"gitlab.com/Pereselkov777/checkurl_cluster/internal/app/database"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"sync"
	"time"
)

type Response struct {
	StatusCode string
	Time       time.Time
}

type Params struct {
	frequency    string
	repeat_count string
	url          string
	url_id       string
	check_id     int
}

func TesterCron(w http.ResponseWriter, r *http.Request) {

	var wg sync.WaitGroup

	wg.Add(1)

	go func() {
		defer wg.Done()

		w.Header().Set("Content-Type", "application/json")
		params, _ := url.ParseQuery(r.URL.RawQuery)
		url := params.Get("url")
		repeat, _ := strconv.Atoi(params.Get("repeat_count"))

		intNum, _ := strconv.Atoi(params.Get("frequency"))

		resp, err := http.Get(url)
		code := resp.StatusCode
		fmt.Printf("CODE", code)
		var resp_arr Response
		resp_arr.StatusCode = strconv.Itoa(code)
		resp_arr.Time = time.Now()
		defer resp.Body.Close()
		if err != nil {
			fmt.Println("err donload File", err)
		}
		s := gocron.NewScheduler(time.UTC)
		s.TagsUnique()
		s.Every(intNum).Minute().Do(taskWithParams, params, resp_arr, 0)

		s.StartAsync()
		if code != 200 && repeat != 0 {
			go ErrorHandler(params, repeat, url)

		}
	}()
	wg.Wait()
	json.NewEncoder(w).Encode(map[string]interface{}{
		"result": "ok",
	})

}
func taskWithParams(params url.Values, resp_arr Response, repeat int) {

	check_id := params.Get("check_id")

	url_id := params.Get("url_id")
	http_code := resp_arr.StatusCode

	config :=
		database.Config{
			ServerName: os.Getenv("DB_HOST") + ":3306",
			User:       os.Getenv("DB_USERNAME"),
			Password:   os.Getenv("DB_PASSWORD"),
			DB:         os.Getenv("DB_DATABASE"),
		}

	connectionString := database.GetConnectionString(config)
	err := database.Connect(connectionString)
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("CONN", connectionString)
	var sql string
	sqlDB, err := database.Connector.DB()
	fmt.Printf("DB CONN OPENED", sqlDB)
	if repeat != 0 {
		fmt.Printf("NO o", repeat)
		sql = "INSERT INTO checklog SET  " +
			"check_id = ?," +
			"http_code = ?," +
			"url_id = ?," +
			"errors_count = ?"
		str := strconv.Itoa(repeat)
		res, err := sqlDB.Exec(sql,
			check_id,
			http_code,
			url_id, str)
		if err != nil {
			fmt.Printf("err", err)
		}
		fmt.Printf("RSS", res)
	} else {
		fmt.Printf("YES o", repeat)
		sql = "INSERT INTO checklog SET  " +
			"check_id = ?," +
			"http_code = ?," +
			"url_id = ?"
		sqlDB.Exec(sql,
			check_id,
			http_code,
			url_id)
	}
	defer sqlDB.Close()
	fmt.Printf("SQL CLOSED", sqlDB)

	if err != nil {
		panic(err)
	}

	fmt.Print("Gocron is finished")

}

func ErrorHandler(params url.Values, repeat int, url string) {
	for i := 1; i <= repeat; i++ {
		fmt.Printf("error_reapiting", i)
		time.Sleep(1 * time.Minute)
		res, errors := http.Get(url)
		if errors != nil {
			fmt.Printf("errors", errors)
		}
		var res_arr Response
		http := res.StatusCode
		res_arr.StatusCode = strconv.Itoa(http)
		res_arr.Time = time.Now()
		taskWithParams(params, res_arr, i)
	}
}

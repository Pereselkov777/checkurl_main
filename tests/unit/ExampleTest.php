<?php

class ExampleTest extends \Codeception\Test\Unit
{
    public $config;
    public function _before()
    {
        $this->config = [
            "frequency" => 5,

            "repeat_count" => 0,

            "url" => "https://google.com",

            "url_id"   => 48,
            "check_id" => 55,
        ];

        parent::_before(); // TODO: Change the autogenerated stub
    }

    public function testRequest()
    {
        $request = new \app\models\Request($this->config);
        $response = $request->getResponse(\app\models\Request::URL_CRON);
        $this->assertTrue(isset($response['result']));
    }
    public function test404()
    {
        $request = new \app\models\Request($this->config);
        $response = $request->getResponse('/wrong_address');
        $this->assertTrue($response == 404);

    }
}
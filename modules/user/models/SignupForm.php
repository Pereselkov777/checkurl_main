<?php

namespace app\modules\user\models;
 
use yii\base\Model;
use Yii;
 
/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $verifyCode;
 
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'match', 'pattern' => '#^[\w_-]+$#i'],
            ['username', 'unique', 'targetClass' => User::className(),
             'message' => 'This username has already been taken.'
            ],
            ['username', 'string', 'min' => 2, 'max' => 255],
 
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => User::className(),
             'message' => 'This email address has already been taken.'
            ],
 
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
 
          //  ['verifyCode', 'captcha', 'captchaAction' => '/user/default/captcha'],
        ];
    }
 
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if(!$this->validate()){
            foreach ($this->getErrors() as $error) {
                Yii::$app->getSession()->setFlash('danger', $error[0]);
            }
            return false;
        }
        $user           = new User();
        $user->username = \yii\helpers\Html::encode($this->username);
        $user->email    = \yii\helpers\Html::encode($this->email);
        $user->setPassword(\yii\helpers\Html::encode($this->password));
        $user->status = User::STATUS_ACTIVE;
        $user->generateAuthKey();
        $user->created_at = time();
        $user->updated_at = time();
        if (!$user->save()) {
            foreach ($user->getErrors() as $error) {
                Yii::$app->getSession()->setFlash('danger', $error[0]);
            }

            return false;
        }
        return true;

    }
}
<?php



return  [
    'class' => \mikemadisonweb\rabbitmq\Configuration::class,
    'logger' => [
        'log' => true,
        'category' => 'application',
        'print_console' => true,
        'system_memory' => false,
    ],
    'connections' => [
        [
            // You can pass these parameters as a single `url` option: https://www.rabbitmq.com/uri-spec.html
            'host' => 'localhost',
            'port' => 5672,
            'user' => 'admin',
            'password' => '12345',
            'vhost' => '/',
        ]
        // When multiple connections is used you need to specify a `name` option for each one and define them in producer and consumer configuration blocks
    ],
    'producers' => [
        [
            'name' => 'DEMO_PRODUCER',
        ],
    ],
    'exchanges' => [
        [
            'name' => 'DEMO_EXCHANGE',
            'type' => 'direct'
        ],
    ],
    'queues' => [
        [
            'name' => 'DEMO_QUEUES',
        ],
    ],
    'bindings' => [
        [
            'queue' => 'DEMO_QUEUES',
            'exchange' => 'DEMO_EXCHANGE',
            'routing_keys' => ['DEMO_ROUTING_KEY','DEMO_ROUTING_KEY2'],
        ],
    ],
    'consumers'         => [
        [
            'name'      => 'consumer-name',
            'callbacks' => [
                'DEMO_QUEUES' => 'rabbitmq.example.consumer',
            ],
        ],
    ],

];
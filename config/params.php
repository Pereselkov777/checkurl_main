<?php

return [
    'domain' => getenv('APP_URL'),
    'main_host' => getenv('APP_MAIN_HOST'),
    'adminEmail' => 'admin@example.com',
    'supportEmail' => getenv('APP_MAILER_USERNAME_SUPPORT'),
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'worker-access-token' => getenv("WORKER_ACCESS_TOKEN"),
];
